# BitBucket Backup #
BitBucket Backup is an Atlassian On-Demand git backup for the Bash-minded.

It will call the BitBucket API to clone a list of all your repos them upload them to s3.

Logs: list of all your repos and a list of changes between updates.

Dockerfile still in progress.


## Dependencies: ##
* jq
* aws cli (See: Supplemental Info)
* bitbucket ssh keys loaded in agent (See: Configuring multiple SSH identities)

## Usage: ##
```
bash +x bitbucket.sh gitusername origin s3bucket awsprofile
```
You will be prompted for a password to make the BitBucket API call

Confusing?  i.e.
```
git clone https://<gitusername>@bitbucket.org/<origin>/repo.git
aws s3 ls s3://<s3bucket>
cat ~/.aws/config
[profile <awsprofile>]
```


## Prepartion ##
Install jq: `curl -O http://stedolan.github.io/jq/download/linux64/jq && chmod +x jq && mv jq /usr/local/bin`

Highly recommend configuring multiple SSH identities and a 'backup' AWS profile.

Configuring multiple SSH identities:
```
ssh-keygen -f ~/.ssh/bitbucket -C "bitbucketid"
Generating public/private rsa key pair.
Enter passphrase (empty for no passphrase):
Enter same passphrase again:
Your identification has been saved in ~/.ssh/bitbucket.
Your public key has been saved in ~/.ssh/bitbucket.pub.

echo "Host bitbucketid\n\tHostName bitbucket.org\n\tIdentityFile ~/.ssh/bitbucketid" >> ~/.ssh/config
chmod 600 ~/.ssh/config
ssh-add ~/.ssh/bitbucket
Identity added: ~/.ssh/bitbucket (~/.ssh/bitbucket)
ssh-add -l
```
Then bring the public key over to bitbucket.org to install under settings > ssh-keys:
`pbcopy < ~/.ssh/bitbucket.pub`

Configure AWS backup profile
```
aws configure --profile bb_backup
AWS Access Key ID [None]: <yourKey>
AWS Secret Access Key [None]: <yourPw>
Default region name [None]: us-west-2
Default output format [None]:
```

### Supplemental Info ###
Install the AWS CLI (Linux & OS X)
```
curl "https://s3.amazonaws.com/aws-cli/awscli-bundle.zip" -o "awscli-bundle.zip"
unzip awscli-bundle.zip
sudo ./awscli-bundle/install -i /usr/local/aws -b /usr/local/bin/aws
```
**Optional:** You can `sudo pip install awscli` but it comes with a lot of dependencies

Cron w/log example:
```0 02 * * 1-5    root    /bin/bash -x /backuplocation/bitbucket.sh > /backuplocation/`date +\%Y\%m\%d\%H\%M\%S`-backup.log 2>&1```
