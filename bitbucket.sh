#!/bin/bash

#-----------------------------------------------------------
# May 2016
# Maintaner: Andy Motta
# Dependencies: jq, aws cli
# Configure BitBucket SSH Keys on local host
# Then configure AWS backup profile
#-----------------------------------------------------------

#--------------------Global Variables----------------------#
BBUSER=$1
ORIGIN=$2
S3BUCKET=$3
AWSPROFILE=$4

if [[ "$#" -ne 4 ]]; then
    echo "Usage ./bitbucket.sh gitusername atlassianaccount s3bucket awsprofile"
		exit 1
fi

read -s  -p BitBucket\ Password: BBPASS
if [[ -z ${BBPASS} ]]; then
		echo -e "\n"
		read -s  -p BitBucket\ Password: BBPASS
		if [[ -z ${BBPASS} ]]; then
			echo -e "\nNo password supplied"
			exit 1
		fi
fi
echo -e "\n"

BACKLOC="$(pwd)/${ORIGIN}" #backup location
REPOTRACKER="${BACKLOC}/${ORIGIN}_repos.log"
BB_LOG="${BACKLOC}/$(date +%Y%m%d%H%M%S)-bitbucket.log"

#--------------------Docker extras----------------------#
#Load bitbucket SSH keys if running in Docker
#eval $(ssh-agent -s) > /dev/null
#ssh-add id_rsa > /dev/null
#--------------------Docker extras----------------------#

[[ -d ${BACKLOC} ]] || mkdir ${BACKLOC}
[[ -f ${REPOTRACKER} ]] || touch ${REPOTRACKER}

#Generating the current repo list
REPOLIST=$(curl -s --user $BBUSER:$BBPASS https://api.bitbucket.org/1.0/user/repositories | jq '.[] | .resource_uri' | tr -d "\"" | cut -d/ -f5)

# diff with old repo list, put changes in mail body
DIFF=$(diff <(echo "${REPOLIST}") <(cat $REPOTRACKER))
if [ -z "${DIFF}" ]; then
	echo "No changes to ${ORIGIN} found" > ${BB_LOG}
else
	echo "Changes to ${ORIGIN} Repository detected:" > ${BB_LOG}
	echo "${DIFF}" >> ${BB_LOG}
fi

#If git clone list exists, git clone.  Otherwise, git pull
# or just check if the dir exists or not and if it does, pull.  Otherwise, close.
for repo in $(echo "${REPOLIST}") ; do
	if [ ! -d ${BACKLOC}/${repo} ]; then
		cd ${BACKLOC}
		echo "========== Cloning $repo =========="
		#git clone --mirror $repo (for precise working copy)
		git clone git@bitbucket.org:${ORIGIN}/${repo}.git
		OK=$?
	else
		echo "========== Updating $repo =========="
		cd ${BACKLOC}/${repo}
#		git remote update (for mirror)
		git pull --all
		OK=$?
	fi

	if [ $OK -ne 0 ]; then
		echo "Clone or update failed on ${repo}" >> ${BB_LOG}
	fi
done

if [ $? -eq 0 ]; then
	echo "All ${ORIGIN} backups successful" >> ${BB_LOG}
	aws s3 sync --storage-class REDUCED_REDUNDANCY ${BACKLOC} s3://${S3BUCKET}/ --profile ${AWSPROFILE}
fi

cat /dev/null > ${REPOTRACKER}
echo "${REPOLIST}" > ${REPOTRACKER}

exit 0
