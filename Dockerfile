#-----------------------------------------------------------
# ##### Work in Progress #######
# 192.168.1.200 is key vault location
# bitbucket is BitBucket SSH Key
# backup dir then upload to S3 with on timer
# ##### Work in Progress #######
# ToDo: Use aws cli container to bind:mount
#-----------------------------------------------------------

FROM debian:jessie

WORKDIR /root

RUN apt-get update && apt-get install -y curl git && \
    curl -O http://stedolan.github.io/jq/download/linux64/jq && chmod +x jq && mv jq /usr/local/bin && \
    curl -O 192.168.1.200/bitbucket && \
    chmod 0600 bitbucket && \
    mkdir .ssh andylukem && \
    echo -e "Host bitbucket.org\n\tStrictHostKeyChecking no\n" >> ~/.ssh/config

ADD bitbucket.sh /root/bitbucket.sh
